## Follow steps below:

1. kubectl apply -f ingress/install.yaml
2. kubectl apply -f cert-manager/install.yaml
3. kubectl apply -f cert-manager/issuer-prod.yaml
4. kubectl apply -f cert-manager/issuer-stg.yaml
5. kubectl apply -f argocd/namespace.yaml
6. kubectl apply -n argocd -f argocd/install.yaml
7. kubectl apply -f argocd/ingress-cm.yaml

## Steps from here onwards, apps are manage by argocd
8. (make sure argocd cli installed) 
   argocd login {argocdURL} --grpc-web
9. argocd app create apps \
   --dest-namespace argocd \
   --dest-server https://kubernetes.default.svc \
   --repo https://gitlab.com/danny.ong/k8s-argocd-infra.git \
   --path apps
